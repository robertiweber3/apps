## Demo Usage:



a) Modify the Dockerfile:

```yaml
#examples

ARG URL=https://github.com/snyk/cli/archive/refs/heads/master.zip
ARG CODE_NAME=cli

ENV SNYK_API="             "

```

b) Build the container to execute the scan against the "snyk cli" codebase.

```sh
# build docker image

podman build -t snyk .

# run container to get reports

podman run --rm -it --name snyk -d snyk

#offload html reports to pwd

podman cp snyk:/home/snyk .

#container is meant to be built, run scan, send results to snyk account, offload html reports, and destroyed.

podman image prune -f
podman rmi alpine:3.18 -f
podman rmi snyk:latest -f

```

## Modify the Dockerfile snyk commands to your liking:

<p align="center">
  <img src="imgs/cheat-sheet-snyk-cli.png" />
</p>

