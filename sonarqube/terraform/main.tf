terraform {
  required_providers {
    podman = {
      source = "project0/podman"
    }
    docker = {
      source  = "kreuzwerker/docker"
      version = "3.0.2"
    }
  }
}

# Per default connects to local unix socket
provider "podman" {
  // default
  uri = "unix:///run/podman/podman.sock"
}

resource "docker_image" "sonarqube" {
  name = "sonarqube"
  build {
    path = "."
    tag  = ["sonarqube:release"]
    build_arg = {
      foo : "sonarqube"
    }
    label = {
      author : "sonarqube"
    }
  }
}

resource "docker_image" "postgres" {
  name = "postgres"
  build {
    path = "./postgres"
    tag  = ["postgres:release"]
    build_arg = {
      foo : "postgres"
    }
    label = {
      author : "postgres"
    }
  }
}

# connect via ssh
provider "podman" {
  alias    = "ssh"
  uri      = "ssh://<user>@<host>[:port]/run/podman/podman.sock?secure=True"
  identity = "/tmp/ssh_identity_key"
}

variable "users" {
  type = set(string)
  default = [
    "sonarqube",
    "ansible",
  ]
}

resource "sonarqube" "global" {
  global_scope = true
  name         = "global"
  scope_id     = "global"
}

resource "sonarqube" "org" {
  scope_id    = sonarqube.global.id
  name        = "primary"
  description = "Primary organization scope"
}

resource "sonarqube" "project" {
  name                     = "code quality"
  description              = "Sonarqube project"
  scope_id                 = sonarqube.org.id
  auto_create_admin_role   = true
  auto_create_default_role = true
}

resource "sonarqube_user" "user" {
  for_each    = var.users
  name        = each.key
  description = "User resource for ${each.key}"
  account_ids = [sonarqube_account_password.user[each.value].id]
  scope_id    = sonarqube.org.id
}

resource "sonarqube_auth_method_password" "password" {
  name        = "org_password_auth"
  description = "Password auth method for org"
  type        = "password"
  scope_id    = sonarqube.org.id
}

resource "sonarqube_account_password" "user" {
  for_each       = var.users
  name           = each.key
  description    = "User account for ${each.key}"
  type           = "password"
  login_name     = lower(each.key)
  password       = "foofoofoo"
  auth_method_id = sonarqube_auth_method_password.password.id
}

resource "sonarqube_role" "global_anon_listing" {
  scope_id = sonarqube.global.id
  grant_strings = [
    "id=*;type=auth-method;actions=list,authenticate",
    "type=scope;actions=list",
    "id={{account.id}};actions=read,change-password"
  ]
  principal_ids = ["u_anon"]
}

resource "sonarqube_role" "org_anon_listing" {
  scope_id = sonarqube.org.id
  grant_strings = [
    "id=*;type=auth-method;actions=list,authenticate",
    "type=scope;actions=list",
    "id={{account.id}};actions=read,change-password"
  ]
  principal_ids = ["u_anon"]
}
resource "sonarqube_role" "org_admin" {
  scope_id       = "global"
  grant_scope_id = sonarqube.org.id
  grant_strings  = ["id=*;type=*;actions=*"]
  principal_ids = concat(
    [for user in sonarqube_user.user : user.id],
    ["u_auth"]
  )
}

resource "sonarqube_role" "proj_admin" {
  scope_id       = sonarqube.org.id
  grant_scope_id = sonarqube.project.id
  grant_strings  = ["id=*;type=*;actions=*"]
  principal_ids = concat(
    [for user in sonarqube_user.user : user.id],
    ["u_auth"]
  )
}

resource "sonarqube_host_catalog_static" "databases" {
  name        = "databases"
  description = "Database targets"
  # type        = "static"
  scope_id    = sonarqube.project.id
}

resource "sonarqube_host_static" "localhost" {
  type            = "static"
  name            = "localhost"
  description     = "Localhost host"
  address         = "localhost"
  host_catalog_id = sonarqube_host_catalog_static.databases.id
}

resource "sonarqube_host_set_static" "local" {
  type            = "static"
  name            = "local"
  description     = "Host set for local servers"
  host_catalog_id = sonarqube_host_catalog_static.databases.id
  host_ids        = [sonarqube_host_static.localhost.id]
}

resource "sonarqube_target" "ssh" {
  type                     = "tcp"
  name                     = "ssh"
  description              = "SSH server"
  scope_id                 = sonarqube.project.id
  session_connection_limit = -1
  session_max_seconds      = 2
  default_port             = 22
  host_source_ids = [
    sonarqube_host_set_static.local.id
  ]
}

resource "sonarqube_target" "webport" {
  type                     = "tcp"
  name                     = "webport"
  description              = "http port for sonarqube"
  scope_id                 = sonarqube.project.id
  session_connection_limit = -1
  session_max_seconds      = 2
  default_port             = 9000
  host_source_ids = [
    sonarqube_host_set_static.local.id
  ]
}

resource "sonarqube_host_static" "postgres" {
  type        = "static"
  name        = "postgres"
  description = "Private postgres container"
  # DNS set via docker-compose
  address         = "postgres"
  host_catalog_id = sonarqube_host_catalog_static.databases.id
}

resource "sonarqube_host_set_static" "postgres" {
  type            = "static"
  name            = "postgres"
  description     = "Host set for postgres containers"
  host_catalog_id = sonarqube_host_catalog_static.databases.id
  host_ids        = [sonarqube_host_static.postgres.id]
}

resource "sonarqube_target" "postgres" {
  type                     = "tcp"
  name                     = "postgres"
  description              = "postgres server"
  scope_id                 = sonarqube.project.id
  session_connection_limit = -1
  session_max_seconds      = 2
  default_port             = 5432
  host_source_ids = [
    sonarqube_host_set_static.postgres.id
  ]
}

resource "random_shuffle" "group" {
  input = [
    for o in sonarqube_user.user : o.id
  ]
  result_count = floor(length(var.users) / 4)
  count        = floor(length(var.users) / 2)
}

resource "random_pet" "group" {
  length = 2
  count  = length(var.users) / 2
}

resource "sonarqube_group" "group" {
    for_each = {
        for k, v in random_shuffle.group : k => v.id
    }
    name        = random_pet.group[each.key].id
    description = "Group: ${random_pet.group[each.key].id}"
    member_ids = tolist(random_shuffle.group[each.key].result)
    scope_id = sonarqube.org.id
}

output "sonarqube_auth_method_password" {
  value = sonarqube_auth_method_password.password.id
}