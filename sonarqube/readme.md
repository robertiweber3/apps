<p><a target="_blank" href="https://app.eraser.io/workspace/VT8kbHJC0QzMNWhI9DJx" id="edit-in-eraser-github-link"><img alt="Edit in Eraser" src="https://firebasestorage.googleapis.com/v0/b/second-petal-295822.appspot.com/o/images%2Fgithub%2FOpen%20in%20Eraser.svg?alt=media&amp;token=968381c8-a7e7-472a-8ed6-4a6626da5501"></a></p>

# Sonarqube
![diagram-export-11-17-2023-9_29_11-AM.png](/.eraser/VT8kbHJC0QzMNWhI9DJx___LwMfdHxN5TbOs3GbthKdogHwLmz1___Is7YxdU7vmt1IkURH2JBJ.png "diagram-export-11-17-2023-9_29_11-AM.png")



[﻿What is Sonarqube?>](https://docs.sonarqube.org/latest/) 

[﻿What is Podman?>](https://www.redhat.com/en/topics/containers/what-is-podman) 

[﻿Installing Podman>](https://podman.io/getting-started/installation) 

Linux requirements:

```
vm.max_map_count is greater than or equal to 524288
fs.file-max is greater than or equal to 131072
the user running SonarQube can open at least 131072 file descriptors
the user running SonarQube can open at least 8192 threads
```
Configuring those settings: 

```sh
sudo tee /etc/sysctl.d/99-sonarqube-pod.conf<<EOF
[main]
summary=Sonarqube/podman/traefik settings
[sysctl]
vm.max_map_count=524288
fs.file-max=131072
net.ipv4.ip_unprivileged_port_start=80
EOF

sudo sysctl -p /etc/sysctl.d/99-sonarqube-pod.conf

#If the user running SonarQube (sonarqube in this example) does not have the permission 
# to have at least 131072 open descriptors, you must insert this line in /etc/security/limits.conf
sudo tee -a /etc/security/limits.conf<< EOF
sonarqube   -   nofile   131072
sonarqube   -   nproc    8192
EOF
```
>   **Note**
podman-compose commands are ran in this example with the user "sonarqube".  

```sh
#local build

podman network create sonarnet
podman build -t sonarqube .
podman run --rm -it --security-opt=no-new-privileges \
    --network sonarnet -p 9000:9000 --name sonarqube -d sonarqube

#using docker-compose yaml's

podman-compose up -d
```
[﻿http://localhost:9000](http://localhost:9000/) 

admin:admin (will be prompted for new password)

## Spin up an external postgres db for sonarqube to connect to:
Modify .env variables

```sh
podman-compose -f docker-compose-postgres-sonar.yml up -d
```
```sh
#podman.sock for user (traefik ssl frontend)
#to expose podman.sock for traefik
sudo systemctl start podman.socket
systemctl --user enable podman.socket

# verify socket is enabled
podman info | grep -i -A 2 remoteSocket

#test socket connection
sudo curl -H "Content-Type: application/json" --unix-socket \
/var/run/podman/podman.sock http://localhost/_ping
```
## run a local scan of sonarqube itself
```sh
# set up sonar-scanner locally

sudo wget https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-5.0.1.3006-linux.zip
sudo unzip sonar-scanner-cli-5.0.1.3006-linux.zip
sudo mv sonar-scanner-5.0.1.3006-linux/* /opt/sonar-scanner

sudo tee -a /opt/sonar-scanner/conf/sonar-scanner.properties<<\EOF
sonar.host.url=http://localhost:9000
sonar.sourceEncoding=UTF-8
# exclude java without class files
sonar.exclusions=**/*.java
sonar.coverage.exclusions=**/*.java
EOF

export PATH="$PATH:/opt/sonar-scanner/bin"

sonar-scanner -v

# get source code

wget https://github.com/SonarSource/sonarqube/archive/refs/heads/master.zip
unzip master.zip; cd sonarqube-master

# Login to localhost:9000 (admin/admin), change password and create a new project - generate token.
# Example using local install of sonar-scanner

sonar-scanner \
           -Dsonar.projectKey=<project name> \
           -Dsonar.sources=.
           -Dsonar.host.url=http://<sonarqube url>:9000 \
           -Dsonar.login=<insert token value>

# using the sonarscanner docker image

cd /dir/that/has/the/codebase/to/be/scanned

podman run --rm \
           -e SONAR_HOST_URL=http://<sonarqube url>:9000 \
           -e SONAR_LOGIN=<Insert the token here> \
           -v ./:/usr/src:z \
           sonarsource/sonar-scanner-cli \
           -Dsonar.projectKey=<project name> \
```
![sonar](images/sonarqube10.2.0.77647-quality-gate.png "")




<!--- Eraser file: https://app.eraser.io/workspace/VT8kbHJC0QzMNWhI9DJx --->