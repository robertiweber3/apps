version: '3'

volumes:
  keycloak-postgres:
  keycloak-postgres-backups:
  traefik-certificates:

networks:
  keycloak:
    name: keycloak
    driver: bridge

services:
  postgres:
    image: docker.io/bitnami/postgresql:latest
    container_name: postgres
    hostname: postgres-keycloak.io
    ports:
      - ${DB_PORT}:${DB_PORT}
    volumes:
      - /etc/localtime:/etc/localtime:ro
      - keycloak-postgres:/bitnami/postgresql:z
    environment:
      POSTGRESQL_REPLICATION_MODE: master
      POSTGRESQL_USERNAME: ${POSTGRESQL_USERNAME}
      POSTGRESQL_PASSWORD: ${POSTGRESQL_PASSWORD}
      POSTGRESQL_DATABASE: ${POSTGRESQL_DATABASE}
      POSTGRESQL_REPLICATION_USER: ${POSTGRESQL_REPLICATION_USER}
      POSTGRESQL_REPLICATION_PASSWORD: ${POSTGRESQL_REPLICATION_PASSWORD}
    healthcheck:
      test: ["CMD-SHELL", "pg_isready -U postgres -h 127.0.0.1"]
      interval: 10s
      timeout: 5s
      retries: 3
      start_period: 60s
    restart: unless-stopped
    networks:
      - keycloak
      
  keycloak:
    image: quay.io/keycloak/keycloak:latest
    container_name: keycloak
    hostname: keycloak.io
    ports:
      - "8077:8080"
    volumes:
      - /etc/localtime:/etc/localtime:ro
    environment:
      DB_VENDOR: ${DB_VENDOR}
      DB_ADDR: ${DB_ADDR}
      DB_PORT: ${DB_PORT}
      DB_DATABASE: ${POSTGRESQL_DATABASE}
      DB_USER: ${POSTGRESQL_USER}
      DB_PASSWORD: ${POSTGRESQL_PASSWORD}
      KEYCLOAK_USER: ${KEYCLOAK_USER}
      KEYCLOAK_PASSWORD: ${KEYCLOAK_PASSWORD}
      JGROUPS_DISCOVERY_PROTOCOL: ${JGROUPS_DISCOVERY_PROTOCOL}
      JGROUPS_DISCOVERY_PROPERTIES: ${JGROUPS_DISCOVERY_PROPERTIES}
      PROXY_ADDRESS_FORWARDING: ${PROXY_ADDRESS_FORWARDING}
      KEYCLOAK_LOGLEVEL: ${KEYCLOAK_LOGLEVEL}
    healthcheck:
      test: ["CMD", "curl", "-f", "http://localhost:8080/"]
      interval: 10s
      timeout: 5s
      retries: 3
      start_period: 120s
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.keycloak.rule=Host(`keycloak.io`)"
      - "traefik.http.routers.keycloak.service=keycloak"
      - "traefik.http.routers.keycloak.entrypoints=websecure"
      - "traefik.http.services.keycloak.loadbalancer.server.port=8077"
      - "traefik.http.routers.keycloak.tls=true"
      - "traefik.http.routers.keycloak.tls.certresolver=letsencrypt"
      - "traefik.http.services.keycloak.loadbalancer.passhostheader=true"
      - "traefik.http.routers.keycloak.middlewares=compresstraefik"
      - "traefik.http.middlewares.compresstraefik.compress=true"
    restart: unless-stopped
    depends_on:
      postgres:
        condition: service_healthy
      traefik:
        condition: service_healthy
    networks:
      - keycloak

  traefik:
    image: docker.io/traefik:latest
    container_name: traefik
    hostname: traefik.io
    command:
      - "--log.level=WARN"
      - "--accesslog=true"
      - "--api.dashboard=true"
      - "--api.insecure=true"
      - "--ping=true"
      - "--ping.entrypoint=ping"
      - "--entryPoints.ping.address=:8082"
      - "--entryPoints.web.address=:80"
      - "--entryPoints.websecure.address=:443"
      - "--providers.docker=true"
      - "--providers.docker.endpoint=unix:///var/run/docker.sock"
      - "--providers.docker.exposedByDefault=false"
      - "--certificatesresolvers.letsencrypt.acme.tlschallenge=true"
      # Email for Let's Encrypt (replace with yours)
      - "--certificatesresolvers.letsencrypt.acme.email=enter_email@here"
      - "--certificatesresolvers.letsencrypt.acme.storage=/etc/traefik/acme/acme.json"
      - "--metrics.prometheus=true"
      - "--metrics.prometheus.buckets=0.1,0.3,1.2,5.0"
      - "--global.checkNewVersion=true"
      - "--global.sendAnonymousUsage=false"
    volumes:
      - /etc/localtime:/etc/localtime:ro
      - $XDG_RUNTIME_DIR/podman/podman.sock:/var/run/docker.sock:ro
      - traefik-certificates:/etc/traefik/acme:Z
    ports:
      - "80:80"
      - "443:443"
    healthcheck:
      test: ["CMD", "wget", "http://localhost:8082/ping","--spider"]
      interval: 10s
      timeout: 5s
      retries: 3
      start_period: 5s
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.dashboard.rule=Host(`traefik.io`)"
      - "traefik.http.routers.dashboard.service=api@internal"
      - "traefik.http.routers.dashboard.entrypoints=websecure"
      - "traefik.http.services.dashboard.loadbalancer.server.port=8080"
      - "traefik.http.routers.dashboard.tls=true"
      - "traefik.http.routers.dashboard.tls.certresolver=letsencrypt"
      - "traefik.http.services.dashboard.loadbalancer.passhostheader=true"
      - "traefik.http.routers.dashboard.middlewares=authtraefik"
      # Passwords must be encoded using MD5, SHA1, or BCrypt
      - "traefik.http.middlewares.authtraefik.basicauth.users=traefikadmin:$$enter$$hashed$$passhere"
      - "traefik.http.routers.http-catchall.rule=HostRegexp(`{host:.+}`)"
      - "traefik.http.routers.http-catchall.entrypoints=web"
      - "traefik.http.routers.http-catchall.middlewares=redirect-to-https"
      - "traefik.http.middlewares.redirect-to-https.redirectscheme.scheme=https"
    restart: unless-stopped
    networks:
      - keycloak

  backups:
    image: docker.io/bitnami/postgresql:latest
    container_name: psql-backup
    hostname: keycloak-backup-psql.io
    command: sh -c 'sleep 30m
             && while true; do
             PGPASSWORD="$$(echo $$POSTGRESQL_DATABASE)"
             pg_dump
             -h postgres-keyloak.io
             -p 5432
             -d "${POSTGRESQL_DATABASE}"
             -U "${POSTGRESQL_USERNAME} | gzip > /srv/keyloak-postgres/backups/keyloak-postgres-backup-$$(date "+%Y-%m-%d_%H-%M").gz
             && find /srv/keyloak-postgres/backups -type f -mtime +7 | xargs rm -f;
             sleep 24h; done'
    volumes:
      - /etc/localtime:/etc/localtime:ro
      - keycloak-postgres:/bitnami/postgresql:z
      - keycloak-postgres-backups:/srv/keycloak-postgres/backups:Z
    environment:
      POSTGRESQL_REPLICATION_MODE: slave
      POSTGRESQL_REPLICATION_USER: ${POSTGRESQL_REPLICATION_USER}
      POSTGRESQL_REPLICATION_PASSWORD: ${POSTGRESQL_REPLICATION_PASSWORD}
      POSTGRESQL_MASTER_HOST: postgres
      POSTGRESQL_PASSWORD: ${POSTGRESQL_PASSWORD}
      POSTGRESQL_MASTER_PORT_NUMBER: 5432
    ports:
      - ${DB_PORT}:${DB_PORT}
    restart: unless-stopped
    depends_on:
      postgres:
        condition: service_healthy
      keycloak:
        condition: service_healthy
    networks:
      - keycloak
