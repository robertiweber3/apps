## Keycloak

<p align="center">
  <img src="https://developers.redhat.com/sites/default/files/styles/article_feature/public/blog/2020/11/2020_Authentication_Author_Keycloak_Featured_Article__B-copy-2.png?itok=geYdEfXu" width="400" />
</p>

One ICAM solution to rule them and in the darkness bind them to just the apps they need for their work function.
