## Site Reliability Engineering (SRE)

Good reads:

https://aws.amazon.com/what-is/sre/

https://sre.google/sre-book/table-of-contents/

https://www.ibm.com/topics/site-reliability-engineering

https://www.redhat.com/en/topics/devops/what-is-sre

https://www.servicenow.com/products/it-operations-management/what-is-site-reliability-engineering.html

https://learn.microsoft.com/en-us/training/paths/az-400-develop-sre-strategy/

https://learn.microsoft.com/en-us/azure/site-reliability-engineering/
