job "caido-init" {
  # Specify this job should run in the region named "us". Regions
  # are defined by the Nomad servers' configuration.
  region = "us"

  # Spread the tasks in this job between us-west-1 and us-east-1.
  datacenters = ["dc1"]

  # Run this job as a "service" type. Each job type has different
  # properties. See the documentation below for more examples.
  type = "service"

  # Specify this job to have rolling updates, two-at-a-time, with
  # 30 second intervals.
  update {
    stagger      = "30s"
    max_parallel = 1
  }

  # A group defines a series of tasks that should be co-located
  # on the same client (host). All tasks within a group will be
  # placed on the same host.
  group "caido-group" {
    # Specify the number of these tasks we want.
    count = 3

    # Create an individual task (unit of work). This particular
    # task utilizes a podman container to front a web application.
    task "podman-driver" {
      # Specify the driver to be "podman". Nomad supports
      # multiple drivers.
      driver = "podman"

      # Configuration is specific to each driver.
      config {
        image = "quay.io/rootshifty/caido:release"

        auth {
          username = "$USERNAME"
          password = "$REPO_PASS"
          server_address  = "quay.io"
        }

        port_map {
          http = 8080
        }
      }

      # The service block tells Nomad how to register this service
      # with Consul for service discovery and monitoring.
      service {
        # This tells Consul to monitor the service on the port
        # labelled "http". Since Nomad allocates high dynamic port
        # numbers, we use labels to refer to them.
        port = "http"

        check {
          type     = "http"
          path     = "/"
          interval = "10s"
          timeout  = "2s"
        }
      }

      # Specify the maximum resources required to run the task,
      # include CPU, memory, and bandwidth.
      resources {
        cpu    = 4000 # MHz
        memory = 8192 # MB

        network {
          mbits = 100

         
          port "http" {}
      

         
        }
      }
    }
  }
}
